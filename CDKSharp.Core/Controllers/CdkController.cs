using System.Net.WebSockets;
using System.Text;
using CDKSharp.Common;
using CDKSharp.Common.enums;
using CDKSharp.Common.events;
using CDKSharp.Common.interfaces;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Serilog;
using ILogger = Serilog.ILogger;

namespace CDKSharp.Core.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CdkController : ControllerBase
    {
        private WebSocket _server;
        private const int EventBuffer = 4096;
        private readonly string _connectionId;
        private const string PluginFilter = "*.dll";
        private const string PluginPath = "/Users/ddominguez/Documents/dev/concepts/CDKSharp/CompiledPlugins";

        private readonly ILogger Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
        private PluginManager _pluginManager;
        private Dictionary<NetworkMessageType, Action> ClientMessageCallBacks;
        public CdkController(PluginManager pluginManager)
        {
            ClientMessageCallBacks = new Dictionary<NetworkMessageType, Action>();
            _pluginManager = pluginManager;
            _connectionId = Guid.NewGuid().ToString();
            ConfigureCallBacks();
            Network.OutboundMessageReceived += OnOutboundMessageReceived;
            CdkLog.ErrorLogged += OnLogMessageReceived;
            CdkLog.InfoLogged += OnLogMessageReceived;
            CdkLog.DebugLogged += OnLogMessageReceived;
        }

        private void OnLogMessageReceived(object sender, LogEvent e)
        {
            Network.SendMessageToClient(this, new NetworkEvent
            {
                ClientId = _connectionId,
                EventType = NetworkEventType.ServerMessage,
                MessageType = NetworkMessageType.LogMessage,
                LogLevel = e.Level,
                MessageString = e.Message
            });
        }

        /// <summary>
        /// Object literal call back configuration
        /// </summary>
        private void ConfigureCallBacks()
        {
            ClientMessageCallBacks.Add(NetworkMessageType.LoadPlugins, () =>
            {
                _pluginManager.LoadPlugins(PluginPath, PluginFilter);
                Network.SendMessageToClient(this, new NetworkEvent
                {
                    ClientId = _connectionId,
                    EventType = NetworkEventType.ServerMessage,
                    MessageType = NetworkMessageType.LoadedPlugins,
                    LoadedPlugins =  PluginManager.LoadedPlugins.Values.ToArray()
                });
            });
            ClientMessageCallBacks.Add(NetworkMessageType.RunPlugins, () => { _pluginManager.Run(); });
            ClientMessageCallBacks.Add(NetworkMessageType.GetLoadedPlugins, () =>
            {
                Network.SendMessageToClient(this, new NetworkEvent
                {
                    ClientId = _connectionId,
                    EventType = NetworkEventType.ServerMessage,
                    MessageType = NetworkMessageType.LoadedPlugins,
                    LoadedPlugins = PluginManager.LoadedPlugins.Values.ToArray()
                });
            });
            ClientMessageCallBacks.Add(NetworkMessageType.StopPlugins, () => { _pluginManager.Stop(); });
        }

        [HttpGet("/cdksharp")]
        public async Task Entry(string pluginPath, string pluginFilter)
        {
            if (!HttpContext.WebSockets.IsWebSocketRequest)
            {
                HttpContext.Response.StatusCode = 400;
                return;
            }
            
            _server = await HttpContext.WebSockets.AcceptWebSocketAsync();
            Network.SendClientConnected(this);
            await IncomingEvents();
            _server.Dispose();
        }

        /// <summary>
        /// Async method to wait for incoming events while the connection is live.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        private async Task IncomingEvents()
        {
            var buffer = new byte[EventBuffer];
            var result = await _server.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            while (!result.CloseStatus.HasValue)
            {
                try
                {
                    var nEvent = JsonConvert.DeserializeObject<NetworkEvent>(Encoding.UTF8.GetString(buffer));
                    Logger.Information($"<= {nEvent.EventType}::{nEvent.MessageType}::{nEvent.MessageString}");
                    
                    if (nEvent?.EventType == NetworkEventType.ClientMessage)
                        ClientMessageCallBacks[nEvent.MessageType].Invoke();
                }
                catch (Exception)
                {
                    // ignored
                }

                result = await _server.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            }
            await _server.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
            Network.SendClientDisconnected(this);
            _server = null;
        }
        
        /// <summary>
        /// OutboundMessage event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private async void OnOutboundMessageReceived(object sender, NetworkEvent e) { await SendClientMessage(e); }
        
        /// <summary>
        /// Helper method to serialize and send object to the client as a JSON formatted byte array.
        /// </summary>
        /// <param name="args"></param>
        private async Task SendClientMessage(object args) { await SendClientMessage(JsonConvert.SerializeObject(args)); }
        
        /// <summary>
        /// Encodes a string message as bytes and sends it to the currently connected client.
        /// </summary>
        /// <param name="message"></param>
        private async Task SendClientMessage(string message)
        {
            if (_server.State == WebSocketState.Closed)
                return;
            
            var messageBytes = Encoding.UTF8.GetBytes(message);
            await _server.SendAsync(new ArraySegment<byte>(messageBytes, 0, messageBytes.Length),
                WebSocketMessageType.Text, WebSocketMessageFlags.EndOfMessage, CancellationToken.None);
        }
    }
}