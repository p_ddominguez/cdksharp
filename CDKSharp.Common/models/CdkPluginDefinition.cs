using System;
using CDKSharp.Common.interfaces;
using Newtonsoft.Json;

namespace CDKSharp.Common.models
{
    [Serializable]
    public class CdkPluginDefinition
    {
        public string Name {get; set; }
        public string Path {get; set; }
        public bool IsEnabled { get; set; }
        [JsonIgnore]
        public ICdkPluginEntry PluginEntry { get; set; }
        public string Version {get; set; }
    }
}