using Serilog;

namespace CDKSharp.Common.interfaces;

internal interface IState
{
    void OnStart();
    void OnExit();
    void Update();
    void FixedUpdate();
}

public abstract class State : IState
{
    protected ILogger Logger;
    protected State(ILogger logger)
    {
        Logger = logger;
        OnStart();
    }
    public abstract void OnStart();
    public abstract void OnExit();
    public abstract void Update();
    public abstract void FixedUpdate();
}