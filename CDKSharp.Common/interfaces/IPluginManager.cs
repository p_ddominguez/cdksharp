using System.Threading;

namespace CDKSharp.Common.interfaces;

public interface IPluginManager
{
    int UpdateInterval { get; set; }
    int FixedUpdateInterval { get; set; }
    void Run();
    void Stop();
    void UpdateLoop(CancellationToken cancellationToken);
    void FixedUpdateLoop(CancellationToken cancellationToken);
    void LoadPlugin(string pluginPath);
    void LoadPlugins(string pluginPath, string pluginFilter);
}