using System;

namespace CDKSharp.Common.interfaces;

internal interface IStateMachine
{
    public static event EventHandler SetState;
    public void OnUpdate();
    void FixedUpdate();
}

/// <summary>
/// Helper State Machine implementation. For more complex use cases, the Stateless StateMachine Nuget is recommended. 
/// </summary>
public abstract class StateMachine: IStateMachine
{
    public State CurrentState { get; private set; }
    public State PreviousState { get; set; }

    protected StateMachine(State initialState)
    {
        CurrentState = initialState;
    }
    public virtual void SetState(State newState)
    {
        CurrentState.OnExit();
        PreviousState = CurrentState;
        CurrentState = newState;
    }
    public abstract void OnUpdate();
    public abstract void FixedUpdate();
}   
