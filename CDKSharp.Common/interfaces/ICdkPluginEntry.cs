using System;
using CDKSharp.Common.events;

namespace CDKSharp.Common.interfaces
{
    /// <summary>
    /// WARNING! Are you sure you want to implement this interface and not the abstract impl?
    /// </summary>
    public interface ICdkPluginEntry
    {
        /// <summary>
        /// The Start method is called one time per invocation of the plugin. This method 
        /// </summary>
        void Start(object sender, EventArgs e);
        
        /// <summary>
        /// Called once per UPDATE_INTERVAL. Intended for smaller payloads that we want to send frequently. IE Vector, SimpleState
        /// </summary>
        /// <param name="sender">The invoking class.</param>
        /// <param name="e">The event arguments from the PluginManager that contain a status update from the PluginManager</param>
        void OnUpdate(object sender, InternalEvent e);
        
        /// <summary>
        /// Called once per FIXED_INTERVAL. Intended for larger payloads that we dont want to send as frequently as the Updates.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnFixedUpdate(object sender, InternalEvent e);
    }

    /// <summary>
    /// The abstract class that your plugin should extend. Currently subscribes to the suggested/common core events.
    /// For Stateful plugins you can piggy back the Core Update and Fixed Update loops respectively.
    /// For Stateless plugins you can define your actions in the Run method. IMPORTANT! Start is only called once per
    /// invocation.
    /// </summary>
    public abstract class CdkPluginEntry : ICdkPluginEntry
    {
        protected CdkPluginEntry()
        {
            Network.ClientConnected += OnClientConnected;
            Network.ClientDisconnected += OnClientDisconnected;
            Network.InboundMessageReceived += OnApiInboundMessageReceived;
            PluginManager.Update += OnUpdate;
            PluginManager.FixedUpdate += OnFixedUpdate;
            PluginManager.PluginsStarted += OnPluginStarted;
            PluginManager.PluginsStopped += OnPluginStopped;
        }
        private void OnPluginStarted(object sender, EventArgs e) { Start(sender, e); }

        public abstract void OnPluginStopped(object sender, EventArgs e);
        public abstract void OnClientConnected(object sender, NetworkEvent e);
        public abstract void OnClientDisconnected(object sender, NetworkEvent e);
        public abstract void OnApiInboundMessageReceived(object sender, NetworkEvent e);
        public abstract void OnUpdate(object sender, InternalEvent e);
        public abstract void OnFixedUpdate(object sender, InternalEvent e);
        public abstract void Start(object sender, EventArgs e);
    }
}