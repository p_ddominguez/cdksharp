using System;

namespace CDKSharp.Common.events
{
    public class Network
    {
        public static event EventHandler<NetworkEvent> ClientConnected;
        public static event EventHandler<NetworkEvent> InboundMessageReceived;
        public static event EventHandler<NetworkEvent> OutboundMessageReceived;
        public static event EventHandler<NetworkEvent> ClientDisconnected;

        public static void ReceiveMessageFromClient(object sender, NetworkEvent args) { InboundMessageReceived?.Invoke(sender, args); }
        public static void SendMessageToClient(object sender, NetworkEvent args) { OutboundMessageReceived?.Invoke(sender, args); }
        public static void SendClientConnected(object sender) { ClientConnected?.Invoke(sender, new NetworkEvent());}
        public static void SendClientDisconnected(object sender) { ClientDisconnected?.Invoke(sender, new NetworkEvent());}
    }
}