using System;
using CDKSharp.Common.enums;
using CDKSharp.Common.models;
using Newtonsoft.Json;

namespace CDKSharp.Common.events
{
    [Serializable]
    public class NetworkEvent : EventArgs
    {
        public string ClientId { get; set; }
        [JsonProperty]
        public NetworkEventType EventType { get; set; }
        [JsonProperty]
        public NetworkMessageType MessageType { get; set; }

        [JsonProperty] 
        public CdkPluginDefinition[] LoadedPlugins { get; set; }
        public LogLevel LogLevel { get; set; }
        public string MessageString { get; set; }
    }
}