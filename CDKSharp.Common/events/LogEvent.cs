using System;
using CDKSharp.Common.enums;

namespace CDKSharp.Common.events;

public class LogEvent : EventArgs
{
    public LogLevel Level { get; set; }
    public string Message { get; set; }
}