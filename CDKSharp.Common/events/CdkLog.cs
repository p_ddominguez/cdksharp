using System;
using CDKSharp.Common.enums;

namespace CDKSharp.Common.events;

public class CdkLog
{
    public static event EventHandler<LogEvent> ErrorLogged;
    public static event EventHandler<LogEvent> DebugLogged;
    public static event EventHandler<LogEvent> InfoLogged;

    public static void LogError(object sender, string message) { ErrorLogged?.Invoke(sender, new LogEvent{Level = LogLevel.Error, Message = message}); }
    public static void LogDebug(object sender, string message) { DebugLogged?.Invoke(sender, new LogEvent{Level = LogLevel.Debug, Message = message}); }
    public static void LogInfo(object sender, string message) { InfoLogged?.Invoke(sender, new LogEvent{Level = LogLevel.Info, Message = message}); }
}
