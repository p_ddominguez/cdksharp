using System;
using System.Collections.Generic;
using CDKSharp.Common.interfaces;
using CDKSharp.Common.models;

namespace CDKSharp.Common.events;

public class InternalEvent : EventArgs
{
    public State CurrentState { get; set; }
    public State PreviousState { get; set; }
    public Dictionary<string, CdkPluginDefinition> LoadedPlugins { get; set; }
}