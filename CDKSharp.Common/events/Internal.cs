using System;

namespace CDKSharp.Common.events;

public class Internal
{
    public static event EventHandler<InternalEvent> Update;
    public static event EventHandler<InternalEvent> FixedUpdate;

    public static void CoreUpdate(object sender, InternalEvent args) { Update?.Invoke(sender, args); }
    public static void CoreFixedUpdate(object sender, InternalEvent args) { FixedUpdate?.Invoke(sender, args); }
}
