﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.Loader;
using System.Threading;
using System.Threading.Tasks;
using CDKSharp.Common.events;
using CDKSharp.Common.interfaces;
using CDKSharp.Common.models;
using Serilog;
using ILogger = Serilog.ILogger;

namespace CDKSharp.Common
{
    public class PluginManager : IPluginManager
    {
        private static readonly ILogger Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();
        public static Dictionary<string, CdkPluginDefinition> LoadedPlugins = new Dictionary<string, CdkPluginDefinition>();
        private CancellationTokenSource _updateCancelTokenSource;
        private CancellationTokenSource _fixedUpdateCancelTokenSource;
        public static event EventHandler<InternalEvent> Update;
        public static event EventHandler<InternalEvent> FixedUpdate;
        public static event EventHandler PluginsStarted;
        public static event EventHandler PluginsStopped;
        public int UpdateInterval { get; set; }
        public int FixedUpdateInterval { get; set; }

        public PluginManager()
        {
            UpdateInterval = 1000;
            FixedUpdateInterval = 2000;
            Network.ClientDisconnected += OnClientDisconnected;
        }

        public PluginManager(int updateInterval, int fixedUpdateInterval)
        {
            UpdateInterval = updateInterval;
            FixedUpdateInterval = fixedUpdateInterval;
            Network.ClientDisconnected += OnClientDisconnected;
        }
        public async void Run()
        {
            _updateCancelTokenSource = new CancellationTokenSource();
            _fixedUpdateCancelTokenSource = new CancellationTokenSource();
            var updateCancelToken = _updateCancelTokenSource.Token;
            var fixedUpdateCancelToken = _fixedUpdateCancelTokenSource.Token;
            PluginsStarted?.Invoke(this, EventArgs.Empty);
            try
            {
                //Note: If you decide to add more threads to the plugin manager for some reason, do so before the await.
                Task.Run(() => UpdateLoop(updateCancelToken), updateCancelToken);
                await Task.Run(() => FixedUpdateLoop(fixedUpdateCancelToken), fixedUpdateCancelToken);
            }
            catch (OperationCanceledException ex)
            {
                Logger.Information($"Stopping plugins...");
                CdkLog.LogInfo(this, $"Stopping plugins...");
            }
            finally
            {
                _updateCancelTokenSource.Dispose();
                _fixedUpdateCancelTokenSource.Dispose();
            }
        }
        public void Stop()
        {
            PluginsStopped?.Invoke(this, EventArgs.Empty);
            _fixedUpdateCancelTokenSource.Cancel();
            _updateCancelTokenSource.Cancel();
        }
        private void OnClientDisconnected(object sender, NetworkEvent e)
        {
            Stop();
        }
        
        public void UpdateLoop(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                Update?.Invoke(this, new InternalEvent());
                Thread.Sleep(UpdateInterval);
            }
        }
        public void FixedUpdateLoop(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                FixedUpdate?.Invoke(this,new InternalEvent());
                Thread.Sleep(FixedUpdateInterval);
            }
        }
        public void LoadPlugin(string pluginPath)
        {
            try {
                AssemblyLoadContext assemblyLoadContext = new AssemblyLoadContext(pluginPath);
                Assembly pluginAssembly = assemblyLoadContext.LoadFromAssemblyPath(pluginPath);
                FileVersionInfo pluginInfo = FileVersionInfo.GetVersionInfo(pluginPath);
                if(!pluginInfo.FileName.Contains("CDKSharp.Common.dll"))
                {
                    ICdkPluginEntry plugin = Activator.CreateInstance(pluginAssembly.GetTypes()[0]) as ICdkPluginEntry;
                    if (pluginInfo.ProductName != null)
                        LoadedPlugins.Add(pluginInfo.ProductName, new CdkPluginDefinition
                        {
                            Name = pluginInfo.ProductName,
                            Version = pluginInfo.FileVersion,
                            PluginEntry = plugin,
                            Path = pluginPath
                        });
                }
            }
            catch (Exception) { }      
        } 
        public void LoadPlugins(string pluginPath, string pluginFilter) 
        {
            foreach (var pluginContext in Directory.GetFiles(pluginPath, pluginFilter))
            {
                try
                {
                    var assemblyLoadContext = new AssemblyLoadContext(pluginContext);
                    var pluginAssembly = assemblyLoadContext.LoadFromAssemblyPath(pluginContext);
                    var pluginInfo = FileVersionInfo.GetVersionInfo(pluginContext);
                    if (!pluginInfo.FileName.Contains("CDKSharp.Common.dll"))
                    {
                        if(!LoadedPlugins.ContainsKey(pluginInfo.ProductName))
                        {
                            var plugin =
                                Activator.CreateInstance(pluginAssembly.GetTypes()[0]) as ICdkPluginEntry;

                            if (pluginInfo?.ProductName != null)
                                LoadedPlugins.Add(pluginInfo.ProductName, new CdkPluginDefinition
                                {
                                    Name = pluginInfo.ProductName,
                                    Version = pluginInfo.FileVersion,
                                    PluginEntry = plugin,
                                    Path = pluginContext
                                });
                        }
                        else
                        {
                            Logger.Error($"{pluginInfo.ProductName} is already loaded!");
                            CdkLog.LogDebug(this, $"{pluginInfo.ProductName} is already loaded!");
                        }
                    }
                }
                catch (Exception e)
                {
                    Logger.Error(e.Message);
                    CdkLog.LogDebug(this, e.Message);
                }
            }
        }
    }
}
