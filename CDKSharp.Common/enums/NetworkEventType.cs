using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CDKSharp.Common.enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NetworkEventType
    {
        ServerMessage = 0x1,
        ClientMessage = 0x2,
        PluginMessage = 0x3,
        ConnectMessage = 0x4,
        DisconnectMessage = 0x5,
    }
}