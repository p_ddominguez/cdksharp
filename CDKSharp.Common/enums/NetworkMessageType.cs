using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CDKSharp.Common.enums
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum NetworkMessageType
    {
        Authenticate = 0x1,
        RunPlugins = 0x2,
        StopPlugins = 0x3,
        Update = 0x4,
        FixedUpdate = 0x5,
        GetLoadedPlugins = 0x6,
        LoadedPlugins = 0x7,
        LoadPlugins = 0x8,
        LogMessage = 0x9
    }
}