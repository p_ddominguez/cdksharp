namespace CDKSharp.Common.enums;

public enum LogLevel
{
    Error,
    Debug,
    Info,
}