﻿using CDKSharp.Common.events;
using CDKSharp.Common.interfaces;
using Serilog;

namespace TestPlugin;

public class Main : CdkPluginEntry
{
    private static ILogger Logger = new LoggerConfiguration().WriteTo.Console().CreateLogger();

    public override void Start(object sender, EventArgs e)
    {
        Logger.Information($"{this} running...");
        CdkLog.LogInfo(this,$"{this} running...");
    }

    public override void OnPluginStopped(object sender, EventArgs e) { }
    public override void OnClientConnected(object sender, NetworkEvent e) { }
    public override void OnClientDisconnected(object sender, NetworkEvent e) { }
    public override void OnApiInboundMessageReceived(object sender, NetworkEvent e) { }
    public override void OnUpdate(object sender, InternalEvent e)
    {
        Logger.Information($"{this} update...");
        CdkLog.LogInfo(this,$"{this} update...");
    }

    public override void OnFixedUpdate(object sender, InternalEvent e)
    {
        Logger.Information($"{this} fixed update...");
        CdkLog.LogInfo(this,$"{this} fixed update...");
    }
}
