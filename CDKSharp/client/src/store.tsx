import {configureStore} from '@reduxjs/toolkit';
import AppReducer from "./context/Slice";
import { PluginDefinition } from './models/PluginDefinition';

export enum Pages {
    Home,
    PluginDash
}

export interface AppState {
    isRunning: boolean,
    isConnected: boolean,
    LoadedPlugins: PluginDefinition[],
    LogMessages: string[]
}

export interface RootState {
    app: AppState
}

export default configureStore<RootState>({
    reducer: {
        app: AppReducer
    }
})