export class PluginDefinition {
    Name? : string
    Path? : string
    Version? : string
    IsEnabled?: Boolean
}