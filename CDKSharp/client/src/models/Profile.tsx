import { PluginDefinition } from "./PluginDefinition"

export class Profile {
    Name? : string
    Plugins? : PluginDefinition[]
}