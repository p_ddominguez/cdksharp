import { Button, Layout, Menu, message, Popover, Tooltip } from 'antd';
import Home from './Pages/Home';
import { useDispatch, useSelector } from 'react-redux';
import { addLogMessage, setIsConnected, setIsRunning, setLoadedPlugins } from '../context/Slice';
import { GetPlugins, INetworkMessage, LoadPlugins, MessageType, RunPlugins, StopPlugins } from '../messages/Network';
import Selectors from '../context/Selectors';
import { useEffect, useState } from 'react';
import { PluginDefinition } from '../models/PluginDefinition';
import { Console } from 'console';
import CdkConsole from './CDKConsole';

const { SubMenu } = Menu;
const { Sider } = Layout;
const ws = new WebSocket("wss://localhost:7294/cdksharp/");

const CDKLayout = () => {
    const dispatch = useDispatch();

    const OnLoadPluginsClicked = () => { ws.send(JSON.stringify(LoadPlugins)); }
    //TODO: The running state should be managed by an incoming message from the core to avoid sync issues.
    const OnRunPluginsClicked = () => { ws.send(JSON.stringify(RunPlugins)); dispatch(setIsRunning(true)); }
    const OnStopPluginsClicked = () => { ws.send(JSON.stringify(StopPlugins)); dispatch(setIsRunning(false)); }
    const [loadedPlugins, SetLoadedPlugins] = useState<PluginDefinition[]>([]);

    useEffect(() => {
        ws.onopen = () => { dispatch(setIsConnected(true)); ws.send(JSON.stringify(GetPlugins)); };
        ws.onclose = () => { dispatch(setIsConnected(false)); }
        ws.onmessage = (e: any) => {
            var nMsg: INetworkMessage = JSON.parse(e.data);
    
            //Log Messages TODO: Break this up into Service Layer
            if (nMsg.MessageType === 'LogMessage')
            {
                dispatch(addLogMessage(nMsg.MessageString));
            }

            //Log Messages TODO: Break this up into Service Layer
            if (nMsg.MessageType === 'LoadedPlugins')
                if (nMsg.LoadedPlugins !== null)
                    SetLoadedPlugins(nMsg.LoadedPlugins);
        }
    },[]);


    return (
        <Layout>
            <Menu theme="dark" mode="horizontal">
                <SubMenu key="1" title="File">
                    <Menu.Item key="1a">Load Profile</Menu.Item>
                    <Menu.Item key="2a">Save Profile</Menu.Item>
                    <Menu.Item key="3a">Export Profile</Menu.Item>
                </SubMenu>
            </Menu>
            <Layout style={{ height: '100vh', width: '100vw' }}>
                <Sider width={200} >
                    <Menu
                        title='Loaded Plugins'
                        mode="inline"
                        theme='light'
                        style={{ height: '100%', borderRight: 0 }}>
                        <Menu.ItemGroup title='Plugin Controls'>
                            <Menu.Item key="1b" >
                                <Button disabled={!useSelector(Selectors.isConnected)} onClick={OnLoadPluginsClicked}>Load Plugins</Button>
                            </Menu.Item>
                            <Menu.Item key="4b" >
                                <Button disabled={!useSelector(Selectors.isConnected)}
                                    onClick={OnRunPluginsClicked}>Run Plugins</Button>
                            </Menu.Item>
                            <Menu.Item key="5b">
                                <Button danger disabled={!useSelector(Selectors.isConnected)}
                                    onClick={OnStopPluginsClicked}>Stop Plugins</Button>
                            </Menu.Item>
                        </Menu.ItemGroup>
                        <Menu.ItemGroup title='Loaded Plugins'>
                            {loadedPlugins.map((x: PluginDefinition) => 
                                <Menu.Item key={x.Name}>
                                    <Popover title={x.Name} content={<><p>Path: {x.Path}</p><p>Version: {x.Version}</p><p>Enabled: {x.IsEnabled}</p><p>State: {x.IsEnabled}</p></>}>
                                        {x.Name}
                                    </Popover>
                                </Menu.Item>)
                            }
                        </Menu.ItemGroup>
                    </Menu>
                </Sider>
                <Layout style={{ padding: '25px', overflowY: 'scroll' }}>
                    <div>
                        <Home />
                        <CdkConsole messages={useSelector(Selectors.GetLogMessages)}/>
                    </div>
                </Layout>
            </Layout>
        </Layout>
    );
}

export default CDKLayout;