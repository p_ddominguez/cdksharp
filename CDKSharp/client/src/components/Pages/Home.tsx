import { Alert, Card, message } from "antd";
import Layout, { Content, Header } from "antd/lib/layout/layout";
import { useSelector } from "react-redux";
import Selectors from "../../context/Selectors";

export interface HomeProps { }

const Home = (props: HomeProps) => {
    const cusStyle : React.CSSProperties = {
        flexGrow:'1',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        color:'rgba(255, 130, 10, 1)',
        fontSize:'32px',
        fontWeight:'bold',
        borderRadius:'3px'
    }
    return (
        <>
            {!useSelector(Selectors.isConnected) && <Alert message="No network connection! Check if the server is running and/or refresh the page." type="error"/>}
        </>
    );
}

export default Home;