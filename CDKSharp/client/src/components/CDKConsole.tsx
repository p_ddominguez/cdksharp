import { Header } from "antd/lib/layout/layout";
import { useSelector } from "react-redux";
import Selectors from "../context/Selectors";

export interface CdkConsoleProps {
    messages: string[]
}

const CdkConsole = (props: CdkConsoleProps) => {
    const {messages} = props;
    const consoleStyle: React.CSSProperties = {
        display:'flex',
        flexDirection:'column',
        background:'rgba(230,247,255, 1)',
        minHeight: '200px',
        maxHeight: '200px',
        overflow: 'scroll',
        color:'rgb(0,21,41)',
        fontWeight: 'bold',
        fontFamily:'monospace',
        borderRadius:'3px',
        padding:'0px 0px 0px 10px',
        margin: '2px 0px 0px 0px'
    }
        
    const cusStyle : React.CSSProperties = {
        flexGrow:'1',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'left',
        color:'rgba(254,253,245, 1)',
        fontSize:'22px',
        fontWeight:'bold',
        borderRadius:'3px'
    }
    return(
        <div style={{display:'flex', flexDirection:'column'}}>
            <Header style={cusStyle}>Event Log</Header>
            <div style={consoleStyle}>{messages.map((m: string) => <div>{m}</div>)}</div>
        </div>
    );
}

export default CdkConsole;