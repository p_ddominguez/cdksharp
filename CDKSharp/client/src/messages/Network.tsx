export interface INetworkMessage {
    ClientId: string,
    EventType: number,
    MessageType: any,
    MessageString?: string,
    LoadedPlugins?: any,
    LogLevel?: any,
}

export enum MessageType {
    Authenticate = 1,
    Run = 2,
    Stop = 3,
    Update = 4,
    FixedUpdate = 5,
    GetPlugins = 6,
    LoadedPlugins = 7,
    Load = 8,
    LogMessage = 9,
}

const LoadPlugins: INetworkMessage = {
    ClientId: "ReactUI",
    EventType: 2,
    MessageType: MessageType.Load
}
    
const RunPlugins: INetworkMessage = {
    ClientId: "ReactUI",
    EventType: 2,
    MessageType: MessageType.Run
}

const GetPlugins: INetworkMessage = {
    ClientId: "ReactUI",
    EventType: 2,
    MessageType: MessageType.GetPlugins
}

const StopPlugins: INetworkMessage = {
    ClientId: "ReactUI",
    EventType: 2,
    MessageType: MessageType.Stop
}

const ClientUpdate: INetworkMessage = {
    ClientId: "ReactUI",
    EventType: 2,
    MessageType: MessageType.Update,
    MessageString: "Test String",
}

export {LoadPlugins, RunPlugins, GetPlugins, StopPlugins, ClientUpdate};
