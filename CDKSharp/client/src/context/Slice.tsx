import { createSlice } from '@reduxjs/toolkit';
import { PluginDefinition } from '../models/PluginDefinition';
import { AppState } from '../store';

const InitialState: AppState = {
    isRunning: false,
    isConnected: false,
    LoadedPlugins: [],
    LogMessages: []
}

export const slice = createSlice({
    name: 'cdksharp-react',
    initialState: InitialState,
    reducers: {
        setIsRunning: (state, action) => { state.isRunning = action.payload },
        setIsConnected: (state, action) => { state.isConnected = action.payload },
        setLoadedPlugins: (state, action) => { state.LoadedPlugins = action.payload},
        addLogMessage: (state, action) => {state.LogMessages.push(action.payload)}
    }
}); 

export const { setIsRunning, setIsConnected, setLoadedPlugins, addLogMessage } = slice.actions;
export default slice.reducer;