import { PluginDefinition } from "../models/PluginDefinition";
import { RootState } from "../store";

export interface Selectors {
    isRunning: () => boolean,
    isConnected: () => boolean,
    GetLoadedPlugins: () => PluginDefinition[]
    GetLogMessages: () => string[]
}

export default { 
    isRunning: (state: RootState) => state.app.isRunning,
    isConnected: (state: RootState) => state.app.isConnected,
    GetLoadedPlugins: (state: RootState) => state.app.LoadedPlugins, 
    GetLogMessages: (state: RootState) => state.app.LogMessages, 
 } as Selectors;