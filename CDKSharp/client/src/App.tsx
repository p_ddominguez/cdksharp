import './App.css';
import CDKLayout from './components/CDKLayout';

function App() {
  return (
    <CDKLayout/>
  );
}

export default App;
